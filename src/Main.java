import java.util.Arrays;
import java.util.Scanner;

public class Main {

    static String[] id = {"1", "2", "3", "4", "5"};
    static String[] name = {"CocaCola", "Fanta", "Sprite", "Orange Joice", "Milk"};
    static String[] price = {"2500", "2000", "2500", "4000", "6000"};

    public static void main(String[] args) {

        display();
        Scanner sc = new Scanner(System.in);
        System.out.println("Please select the drink you want:");
        int selectedDrink = sc.nextInt();
        calculatePrice(selectedDrink);
    }

    static void display() {


        System.out.println("\n\n----------------------");
        System.out.println("DRINK LIST:");
        System.out.println("----------------------");
        String leftAlignFormat = "| %-3s | %-15s | %-8s |%n";

        System.out.format("+-----+-----------------+----------+%n");
        System.out.format("| ID  | NAME            | PRICE    |%n");
        System.out.format("+-----+-----------------+----------+%n");

        for (int i = 0; i < id.length; i++) {
            System.out.format(leftAlignFormat, id[i], name[i], price[i]);
        }
        System.out.format("+-----+-----------------+----------+%n");
    }

    static void calculatePrice(int id) {
        System.out.println("Your order is: " + name[id - 1] +" = "+ price[id - 1]);
        System.out.println("Thank you! Please come again next time.");
    }
}
